package it.abenthung.passwordhasher.controller;

import java.util.Random;

import org.apache.commons.codec.digest.Crypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;


import it.abentuhng.passwordhasher.beans.Passwords;

@Controller
public class PwHashController {
	private static final Logger logger = LoggerFactory.getLogger(PwHashController.class);
	
	@Autowired Random rg;
	
	/**
	 * https://stackoverflow.com/a/25483806/1219104
	 */
	static final String SALT_CHARS = "./0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	@PostMapping("/")
    public String greetingSubmit(Passwords passwords, Model model) {
		logger.info("Hashanfrage gestellt...");
		
		if(passwords != null && passwords.getPassword() != null && passwords.getPasswordRepeat() != null) {
			if(!passwords.getPassword().equals(passwords.getPasswordRepeat())) {
				model.addAttribute("generatedHash", "Die eingegebenen Passwörter stimmen nicht überein.");
				logger.info("Die eingegebenen Passwörter stimmen nicht überein.");
				return "pwForm";
			}
		}

		if(passwords.getPassword().length() < 8) {
			model.addAttribute("generatedHash", "Passwort muss mindestens 8 Zeichen lang sein.");
			logger.info("Passwort muss mindestens 8 Zeichen lang sein.");
			return "pwForm";
		}
		// https://stackoverflow.com/a/31403814/1219104

		try {
			final String salt = getSalt();
			final long rounds = 200000L;
			final String crypt = "{SHA512-CRYPT}";
			
			final String hash = crypt + Crypt.crypt(passwords.getPassword(), "$6$rounds="+rounds+"$"+salt);
			logger.info("Hash generiert: "+hash);
			model.addAttribute("generatedHash", hash);
			
		}catch(Exception e) {
			model.addAttribute("generatedHash", "Bei der Verarbeitung ist ein Fehler aufgetreten.");
			logger.error("Bei der Verarbeitung ist ein Fehler aufgetreten.", e);
		}
		
		
		return "pwForm";
        
    }

	
	@GetMapping("/")
	public String index(Model model) {
		logger.info("Password-hasher aufgerufen.");
		return "pwForm";
	}
	
	private String getSalt() {
		String salt = "";
		while(salt.length() < 16) {
			int next = rg.nextInt(SALT_CHARS.length());
			salt += SALT_CHARS.charAt(next);
		}
		return salt;
	}
	

}
