package it.abenthung.passwordhasher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PasswordHasherApplication {

	public static void main(String[] args) {
		SpringApplication.run(PasswordHasherApplication.class, args);
	}

}
