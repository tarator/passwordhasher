package it.abenthung.passwordhasher;

import java.util.Random;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

	@Bean(name="randomGenerator")
	Random getRandomGenerator() {
		return new Random(System.currentTimeMillis());
	}
}
