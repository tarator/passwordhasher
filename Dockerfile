FROM ubuntu:18.04

# Set timezone
#ENV TIMEZONE=Europe/Vienna
#RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime && echo $TIMEZONE > /etc/timezone


MAINTAINER Georg Abenthung <georg@abenthung.name>
RUN apt-get update && \
	apt-get dist-upgrade -y && \
	apt-get install -y openjdk-8-jre-headless iputils-ping && \
	apt-get clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ENV HOME /javaapp
RUN mkdir /javaapp && mkdir /resources
ADD ./bin/runApp.sh /usr/local/bin
RUN chmod a+x /usr/local/bin/runApp.sh
ADD src/main/resources/* /resources/
ADD target/passwordhasher*.jar /javaapp/
#ADD public /javaapp/public



WORKDIR /javaapp
EXPOSE 8080
VOLUME /resources
CMD ["runApp.sh"]
