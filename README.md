# Password Hasher

Spring-Boot Webapplication for creating Dovecot Password Hashes.

Enter a password and it generates a Hash-String which looks like this:

    {SHA512-CRYPT}$6$rounds=200000$Gjoch37BclVzhR/u$Cu7uUwJKgOHE5VeSX2qo8dkb9W8izhV6VXu0AEwZgjn3bdCpPCvFLciYyjrgQt.iJmRSNQ4crXnsdzvtr5.m3.

You can test a running instance of this application here: [https://passwordhasher.projecttac.com/](https://passwordhasher.projecttac.com/)

